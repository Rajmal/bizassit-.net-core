﻿using DataObjects;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace POS_Nayague_DataAccess_MySql
{
    public class StoreLocator
    {
        private MySqlDatabase MySqlDatabase { get; set; }

        public StoreLocator(MySqlDatabase mySqlDatabase)
        {
            this.MySqlDatabase = mySqlDatabase;
        }

        public async Task<Int32> Save_Store_Locator(StoreLocatorModel store)
        {
            try
            {
                var cmd = this.MySqlDatabase.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"INSERT INTO ng_ps_branch(branch_name,location,Is_main_branch,shop_id) VALUES(@BranchName, @Location, @IsMainBranch, @ShopId)";
                cmd.Parameters.AddWithValue("@BranchName", store.Name);
                cmd.Parameters.AddWithValue("@Location", store.Address);
                cmd.Parameters.AddWithValue("@IsMainBranch", store.IsMainBranch);
                cmd.Parameters.AddWithValue("@ShopId", store.ShopId);

                Int32 shop_saved = await cmd.ExecuteNonQueryAsync();

                return shop_saved;
            }
            catch (MySqlException e)
            {
                return e.Number;
            }
        } 

        public async Task<Int32> Update_Store_Locator(StoreLocatorModel store)
        {
            try
            {
                var cmd = this.MySqlDatabase.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE ng_ps_branch SET branch_name = @BranchName, location = @Location, Is_main_branch = @IsMainBranch, shop_id = @ShopId WHERE id = @Id";
                cmd.Parameters.AddWithValue("@BranchName", store.Name);
                cmd.Parameters.AddWithValue("@Location", store.Address);
                cmd.Parameters.AddWithValue("@IsMainBranch", store.IsMainBranch);
                cmd.Parameters.AddWithValue("@ShopId", store.ShopId);
                cmd.Parameters.AddWithValue("@Id", store.Id);

                Int32 shop_saved = await cmd.ExecuteNonQueryAsync();

                return shop_saved;
            }
            catch (MySqlException e)
            {
                return e.Number;
            }
        }

        public async Task<Int32> Delete_Store_Locator(int id)
        {
            try
            {
                var cmd = this.MySqlDatabase.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"DELETE FROM ng_ps_branch WHERE id = @Id";
                cmd.Parameters.AddWithValue("@Id", id);

                Int32 shop_saved = await cmd.ExecuteNonQueryAsync();

                return shop_saved;
            }
            catch (MySqlException e)
            {
                return e.Number;
            }
        }

        public async Task<Int32> Get_All_Store_Locators_On_Shop(int shopid)
        {
            try
            {
                var cmd = this.MySqlDatabase.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT * FROM ng_ps_branch WHERE shop_id = @ShopId";
                cmd.Parameters.AddWithValue("@ShopId", shopid);

                Int32 shop_saved = await cmd.ExecuteNonQueryAsync();

                return shop_saved;
            }
            catch (MySqlException e)
            {
                return e.Number;
            }
        }
    }
}
