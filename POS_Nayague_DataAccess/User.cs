﻿using DataObjects;
using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS_Nayague_DataAccess_MySql
{

    public class User
    {
        private MySqlDatabase MySqlDatabase { get; set; }

        public User(MySqlDatabase mySqlDatabase)
        {
            this.MySqlDatabase = mySqlDatabase;
        }
        public async Task<UserAuth> checkValidUserAsync(string loginId, string password) 
        {
            var cmd = this.MySqlDatabase.Connection.CreateCommand() as MySqlCommand;
            //cmd.CommandText = @"SELECT id FROM ng_ps_user_login WHERE user_name ='" + loginId+ "' AND user_password='" + password+"'";
            cmd.CommandText = @"SELECT usr.id, usr.user_sub_group_id FROM ng_ps_user_login lg inner join ng_ps_user_detail usr on usr.id = lg.id WHERE user_name = @loginId AND user_password= @password";
            cmd.Parameters.AddWithValue("@loginId", loginId);
            cmd.Parameters.AddWithValue("@password", password);

            var user = new UserAuth();

            using (var reader = await cmd.ExecuteReaderAsync())
            {
                //var hasArecord = reader.HasRows;

                while (await reader.ReadAsync())
                {
                    user.userId = reader.GetFieldValue<Int32>(0).ToString();
                    user.user_group = reader.GetFieldValue<Int32>(1).ToString();
                }

            }

            return user;
        }

        public async Task<Logged_User> getLoggedUserDetails(int user_id)
        {
            var cmd = this.MySqlDatabase.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"SELECT id, CONCAT(first_name,' ',last_name) As user_full_name, user_sub_group_id, COALESCE(user_branch_id,-1) FROM ng_ps_user_detail WHERE id = @user_id ";
            cmd.Parameters.AddWithValue("@user_id", user_id);

            var userDetails = new Logged_User();

            using (var reader = await cmd.ExecuteReaderAsync())
            {
                //var hasArecord = reader.HasRows;

                while (await reader.ReadAsync())
                {
                    userDetails.id = reader.GetFieldValue<Int32>(0);
                    userDetails.Name = reader.GetFieldValue<String>(1);
                    userDetails.user_group = reader.GetFieldValue<Int32>(2);
                    userDetails.branch = reader.GetFieldValue<Int32>(3);
                }

            }
            return userDetails;
        }

        public async Task<IEnumerable<Module>> getEligibleModules(int user_group)
        {
            var cmd = this.MySqlDatabase.Connection.CreateCommand() as MySqlCommand;

            if(user_group == 1)
            {
                cmd.CommandText = @"SELECT DISTINCT md.id, md.module_name FROM ng_ps_module md INNER JOIN ng_ps_menu_item me ON md.id = me.module_id";
            }
            else
            {
                cmd.CommandText = @"SELECT DISTINCT md.id, md.module_name FROM ng_ps_module md INNER JOIN ng_ps_menu_item me ON md.id = me.module_id INNER JOIN ng_ps_user_menu_item mni ON me.id = mni.menu_item_id WHERE mni.user_group_id = @user_group";
                cmd.Parameters.AddWithValue("@user_group", user_group);
            }

            
            
            var userModules = new List<Module>();

            using (var reader = await cmd.ExecuteReaderAsync())
            {
                //var hasArecord = reader.HasRows;

                while (await reader.ReadAsync())
                {
                    var userModule = new Module();
                    userModule.id = reader.GetFieldValue<Int32>(0);
                    userModule.module_name = reader.GetFieldValue<String>(1);
                    userModules.Add(userModule);
                }

            }
            return userModules;
        }

        public async Task<IEnumerable<MenuItem>> getEligibleMenuItems(int user_group)
        {
            var cmd = this.MySqlDatabase.Connection.CreateCommand() as MySqlCommand;

            if(user_group == 1)
            {
                cmd.CommandText = @"SELECT DISTINCT me.id, me.menu_item_name, me.module_id FROM ng_ps_module md INNER JOIN ng_ps_menu_item me ON md.id = me.module_id";
            }
            else
            {
                cmd.CommandText = @"SELECT DISTINCT me.id, me.menu_item_name, me.module_id FROM ng_ps_module md INNER JOIN ng_ps_menu_item me ON md.id = me.module_id INNER JOIN ng_ps_user_menu_item mni ON me.id = mni.menu_item_id WHERE mni.user_group_id = @user_group";
                cmd.Parameters.AddWithValue("@user_group", user_group);
            }
            

            var userMenuItems = new List<MenuItem>();

            using (var reader = await cmd.ExecuteReaderAsync())
            {
                //var hasArecord = reader.HasRows;

                while (await reader.ReadAsync())
                {
                    var userMenuItem = new MenuItem();
                    userMenuItem.id = reader.GetFieldValue<Int32>(0);
                    userMenuItem.menu_item_name = reader.GetFieldValue<String>(1);
                    userMenuItem.module_id = reader.GetFieldValue<Int32>(2);
                    userMenuItems.Add(userMenuItem);
                }

            }
            return userMenuItems;
        }


    }
}
