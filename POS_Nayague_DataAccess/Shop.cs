﻿using DataObjects;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace POS_Nayague_DataAccess_MySql
{
    public class Shop
    {
        private MySqlDatabase MySqlDatabase { get; set; }

        public Shop(MySqlDatabase mySqlDatabase)
        {
            this.MySqlDatabase = mySqlDatabase;
        }

        public async Task<Int32> Save_Shop(ShopModel shp)
        {
            try
            {
                var cmd = this.MySqlDatabase.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"INSERT INTO ng_ps_shop(shop_name,owner_id) VALUES(@ShopName, @Owner_Id)";
                cmd.Parameters.AddWithValue("@ShopName", shp.Name);
                cmd.Parameters.AddWithValue("@Owner_Id", shp.Owner_Id);

                Int32 shop_saved = await cmd.ExecuteNonQueryAsync();

                return shop_saved;
            }
            catch(MySqlException e)
            {
                return e.Number ;
            }
            
        }

        public async Task<Int32> Update_Shop(ShopModel shp)
        {
            try
            {
                var cmd = this.MySqlDatabase.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"Update ng_ps_shop SET shop_name = @ShopName WHERE id = @Shop_Id AND owner_id = @Owner_Id";
                cmd.Parameters.AddWithValue("@ShopName", shp.Name);
                cmd.Parameters.AddWithValue("@Shop_Id", shp.Id);
                cmd.Parameters.AddWithValue("@Owner_Id", shp.Owner_Id);

                Int32 shop_saved = await cmd.ExecuteNonQueryAsync();

                return shop_saved;
            }
            catch (MySqlException e)
            {
                return e.Number;
            }

        }

        public async Task<Int32> Delete_Shop(ShopModel shp)
        {
            try
            {
                var cmd = this.MySqlDatabase.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"DELETE FROM ng_ps_shop WHERE id = @Shop_Id AND owner_id = @Owner_Id";
                cmd.Parameters.AddWithValue("@Shop_Id", shp.Id);
                cmd.Parameters.AddWithValue("@Owner_Id", shp.Owner_Id);

                Int32 shop_saved = await cmd.ExecuteNonQueryAsync();

                return shop_saved;
            }
            catch (MySqlException e)
            {
                return e.Number;
            }

        }

        public async Task<IEnumerable<ShopModel>> Get_All_Shops()
        {
            var cmd = this.MySqlDatabase.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"SELECT * FROM ng_ps_shop";

            var shops = new List<ShopModel>();

            using (var reader = await cmd.ExecuteReaderAsync())
                while (await reader.ReadAsync())
                {
                    var t = new ShopModel()
                    {
                        Id = reader.GetFieldValue<Int32>(0).ToString(),
                        Name = reader.GetFieldValue<String>(1).ToString(),
                        Owner_Id = reader.GetFieldValue<Int32>(2).ToString()
                    };


                    shops.Add(t);
                }

            return shops;
        }

    }
}
