﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace POS_Nayague_Security
{
    public class Password
    {
        public string Encrypt(string decryptText)
        {
            byte[] salt = new byte[128 / 8];
            salt = Encoding.UTF8.GetBytes("sldkfjoij#@*@#W)RESDIS)FIW#W*JW)ERU@#32423$(UWREJRS)EU");

            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: decryptText,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return hashed;
        }
    }
}
