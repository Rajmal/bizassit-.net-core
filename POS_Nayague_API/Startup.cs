using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjects;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using POS_Nayague_DataAccess_MySql;

namespace POS_Nayague_API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.AllowAnyOrigin()
                           .AllowAnyMethod()
                           .AllowAnyHeader();
                });
            });

            string securityKey = "shfdia3y489uy@#@#R*(@#&Wjee8ouweljrw98u@#$@#$";
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        //what to validate
                        ValidateAudience = true,
                        ValidateIssuer = true,
                        ValidateIssuerSigningKey = true,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero,
                        //setup validate data
                        ValidIssuer = "POS_Nayague",
                        ValidAudience = "POS_API",
                        IssuerSigningKey = symmetricSecurityKey
                    };
                });

            services.AddControllers();

            var appsettingConString = Configuration.GetSection("connectionString");

            var ConString = new ConnectionString()
            {
                Server = appsettingConString.GetSection("Server").Value,
                Port = appsettingConString.GetSection("Port").Value,
                Database = appsettingConString.GetSection("Database").Value,
                Username = appsettingConString.GetSection("Username").Value,
                Password = appsettingConString.GetSection("Password").Value
            };

            var CS = "Server=" + ConString.Server + 
                "; Port=" + ConString.Port + 
                "; Database=" + ConString.Database + 
                "; Uid=" + ConString.Username+ 
                "; Pwd=" + ConString.Password + 
                ";";

            //MySql
            services.AddTransient<MySqlDatabase>(_ => new MySqlDatabase(CS));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(MyAllowSpecificOrigins);

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        
    
    }
}
