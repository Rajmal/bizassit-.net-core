﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POS_Nayague_DataAccess_MySql;
using POS_Nayague_Security;

namespace POS_Nayague_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]

    public class ShopController : ControllerBase
    {
        
        public Shop ShopDB { get; set; }

        public ShopController(MySqlDatabase mySqlDatabase)
        {
            ShopDB = new Shop(mySqlDatabase);
        }

        [HttpPost]
        [Authorize(Roles = "1,2")]
        public ActionResult CreateShop(ShopModel shp)
        {
            if (shp.Name == "" || shp.Owner_Id == "")
            {
                return BadRequest();
            }

            try
            {
                shp.Owner_Id = Crypto.Decrypt(shp.Owner_Id);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            var save_shop = ShopDB.Save_Shop(shp).Result;
            return Ok(save_shop);

            //If the result is 1062, Then there is a shop against to the perticular user in the same name.
        }

        [HttpPut]
        [Authorize(Roles = "1,2")]
        public ActionResult UpdateShop(ShopModel shp)
        {
            if (shp.Name.Trim() == "" || shp.Id.Trim() == "" || shp.Owner_Id.Trim() == "")
            {
                return BadRequest();
            }

            try
            {
                shp.Owner_Id = Crypto.Decrypt(shp.Owner_Id);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            var update_shop = ShopDB.Update_Shop(shp).Result;
            return Ok(update_shop);

            //If the result is 1062, Then there is a shop against to the perticular user in the same name.
        }

        [HttpDelete]
        [Authorize(Roles = "1,2")]
        public ActionResult DeleteShop(ShopModel shp)
        {
            if (shp.Id.Trim() == "" || shp.Owner_Id.Trim() == "")
            {
                return BadRequest();
            }

            try
            {
                shp.Owner_Id = Crypto.Decrypt(shp.Owner_Id);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            var delete_shop = ShopDB.Delete_Shop(shp).Result;
            return Ok(delete_shop);

            //If the result is 1062, Then there is a shop against to the perticular user in the same name.
        }

        [HttpGet]
        public ActionResult GetAllShops() 
        {
            var shops = ShopDB.Get_All_Shops().Result;
            return Ok(shops);
        }

    }
}