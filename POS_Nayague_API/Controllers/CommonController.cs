﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DataObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POS_Nayague_DataAccess_MySql;
using System.Net.Http;
using POS_Nayague_Security;

namespace POS_Nayague_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CommonController : ControllerBase
    {
        private User UserDB { get; set; }
        public CommonController(MySqlDatabase mySqlDatabase)
        {
            UserDB = new User(mySqlDatabase);
        }
        
        [HttpGet("GetInitialData")]
        public ActionResult GetData(string userid) 
        {
            int decryptedUserId;

            if (userid == null || userid.Trim() == "")
            {
                return BadRequest();
            }


            try
            {
                decryptedUserId = Int32.Parse(Crypto.Decrypt(userid));
            }
            catch(Exception ex) 
            {
                return BadRequest();
            }

            var userDetails = UserDB.getLoggedUserDetails(decryptedUserId).Result;
            var eligibleModules = UserDB.getEligibleModules(userDetails.user_group).Result;
            var eligibleMenuItems = UserDB.getEligibleMenuItems(userDetails.user_group).Result;

            return Ok(new InnitialData()
            {
                user_details = userDetails,
                modules = eligibleModules,
                menu_items = eligibleMenuItems
            }
            );

        }


    }
}