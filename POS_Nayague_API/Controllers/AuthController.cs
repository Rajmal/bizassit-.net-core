﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DataObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using POS_Nayague_DataAccess_MySql;
using POS_Nayague_Security;

namespace POS_Nayague_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        
        private User UserDB { get; set; }

        public AuthController(MySqlDatabase mySqlDatabase)
        {
            UserDB = new User(mySqlDatabase);
        }

        [HttpPost("login")]
        public ActionResult Login(AuthCredentials auth) 
        {
            //var user = new User(MySqlDatabase);

            if(auth.Username == "" || auth.Password == "")
            {
                return BadRequest();
            }

            var pass = new Password();

            var logged_user = UserDB.checkValidUserAsync(auth.Username, pass.Encrypt(auth.Password)).Result;
            

            if (logged_user.userId == "-1")
            {
                return Ok("Username or password error");
            }
            else
            {
                //var x = new UserAuth();

                logged_user.userId = Crypto.Encrypt(logged_user.userId.ToString());
                logged_user.AuthToken = getToken(false, logged_user.user_group);
                logged_user.RefreshToken = getToken(true, logged_user.user_group);

                logged_user.user_group = Crypto.Encrypt(logged_user.user_group.ToString());

                //var result = JsonConvert.SerializeObject(logged_user);

                return Ok(logged_user);
            }

        }
        /// <summary>
        /// Start Here
        /// </summary>
        /// <returns></returns>
        [HttpPost("token")]
        [Authorize]
        public ActionResult GetToken(string userGroup)
        {
           userGroup = Crypto.Decrypt(userGroup);

            return Ok(new Token()
            {
                AuthToken = getToken(false, userGroup),
                RefreshToken = getToken(true, userGroup)
            });

        }

        private String getToken(bool isrefresh, string loggedUser)
        {
            //security key
            string securityKey = "shfdia3y489uy@#@#R*(@#&Wjee8ouweljrw98u@#$@#$";

            //symetric security key
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));

            //signing credentials
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

            //add claims
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Role, loggedUser.ToString()));
            //claims.Add(new Claim("UserId", userId.ToString()));

            //create token
            JwtSecurityToken token;

            if (isrefresh)
            {
                token = new JwtSecurityToken(
                    issuer: "POS_Nayague",
                    audience: "POS_API",
                    expires: DateTime.Now.AddHours(1),
                    signingCredentials: signingCredentials,
                    claims: claims
                );
            }
            else
            {
                token = new JwtSecurityToken(
                    issuer: "POS_Nayague",
                    audience: "POS_API",
                    expires: DateTime.Now.AddMinutes(1),
                    signingCredentials: signingCredentials,
                    claims: claims
                );
            }

            

            //return token
            return (new JwtSecurityTokenHandler().WriteToken(token));
        }
    }
}