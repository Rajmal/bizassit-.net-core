﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POS_Nayague_DataAccess_MySql;

namespace POS_Nayague_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StoreLocatorController : ControllerBase
    {
        public StoreLocator StoreLocatorDB;

        public StoreLocatorController(MySqlDatabase mySqlDatabase)
        {
            StoreLocatorDB = new StoreLocator(mySqlDatabase);
        }

        [HttpPost]
        public ActionResult CreateStoreLocator(StoreLocatorModel store)
        {
            if(store.Address == "" || store.Name == "" || store.ShopId < 0)
            {
                return BadRequest();
            }

            var save_store_locator = StoreLocatorDB.Save_Store_Locator(store).Result;
            return Ok(save_store_locator);

        }

        [HttpPut]
        public ActionResult UpdateStoreLocator(StoreLocatorModel store)
        {
            if (store.Address == "" || store.Name == "" || store.ShopId < 0)
            {
                return BadRequest();
            }

            var update_store_locator = StoreLocatorDB.Update_Store_Locator(store).Result;
            return Ok(update_store_locator);
        }

        [HttpDelete]
        public ActionResult DeleteStoreLocator(StoreLocatorModel store)
        {
            if (store.Id < 0 )
            {
                return BadRequest();
            }

            var delete_store_locator = StoreLocatorDB.Delete_Store_Locator(store.Id).Result;
            return Ok(delete_store_locator);
        }

        public ActionResult GetAllStoreLocators(StoreLocatorModel store) 
        {
            if (store.ShopId < 0)
            {
                return BadRequest();
            }

            var all_store_locators = StoreLocatorDB.Get_All_Store_Locators_On_Shop(store.ShopId).Result;
            return Ok(all_store_locators);
        }
    }
}