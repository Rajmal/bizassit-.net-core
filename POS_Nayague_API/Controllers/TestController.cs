﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POS_Nayague_Security;

namespace POS_Nayague_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        [HttpGet("password")]
        public ActionResult GetEncryptedPassword(string password)
        {
            var pass = new Password();
            return Ok(pass.Encrypt(password));
        }
    }
}