﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataObjects
{
    public class Logged_User
    {
        public Int32 id { get; set; }
        public String Name { get; set; }
        public Int32 user_group { get; set; }
        public Int32 branch { get; set; }

    }
}
