﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataObjects
{
    public class InnitialData
    {
        public Logged_User user_details { get; set; }
        public IEnumerable<Module> modules { get; set; }
        public IEnumerable<MenuItem> menu_items { get; set; }
    }
}
