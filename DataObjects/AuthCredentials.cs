﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataObjects
{
    public class AuthCredentials
    {
        public String Username { get; set; }
        public String Password { get; set; }

    }
}
