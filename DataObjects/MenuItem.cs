﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataObjects
{
    public class MenuItem
    {
        public Int32 id { get; set; }
        public String menu_item_name { get; set; }
        public Int32 module_id { get; set; }
    }
}
