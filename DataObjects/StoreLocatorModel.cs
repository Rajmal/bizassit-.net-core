﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataObjects
{
    public class StoreLocatorModel
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
        public bool IsMainBranch { get; set; }
        public Int32 ShopId { get; set; }

    }
}
