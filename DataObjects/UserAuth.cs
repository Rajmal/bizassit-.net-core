﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataObjects
{
    public class UserAuth : Token
    {
        public string userId { get; set; }
        public string user_group { get; set; }
    }
}
