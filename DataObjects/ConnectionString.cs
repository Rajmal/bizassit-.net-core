﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataObjects
{
    public class ConnectionString
    {
        public String Server { get; set; }
        public String Port { get; set; }
        public String Database { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }

    }
}
