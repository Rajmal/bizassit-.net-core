﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataObjects
{
    public class Token
    {
        [Required]
        public string AuthToken { get; set; }
        [Required]
        public string RefreshToken { get; set; }
    }
}
