﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataObjects
{
    public class ErrorMsg
    {
        public Int32 Id { get; set; }
        public String Msg { get; set; }
    }
}
